extends Node2D


signal sprite_created(new_sprite)

const Player := preload("res://sprites/PC.tscn")
const Dog := preload("res://sprites/Dog.tscn")
const Floor := preload("res://sprites/Floor.tscn")

var _new_ConvertCoord := preload("res://library/ConvertCoord.gd").new()
var _new_GardenSize := preload("res://library/GardenSize.gd").new()
var _new_GroupName := preload("res://library/GroupName.gd").new()

var _initialized: bool = false

func _ready():
	pass
	

func _process(_delta) -> void:
	if not _initialized:
		_initialized = true
		_init_floor()
		_init_pc()
		_init_dog()
	
	
func _init_floor() -> void:
	for i in range(_new_GardenSize.MAX_X):
		for j in range(_new_GardenSize.MAX_Y):
			_add_sprite(_create_sprite(Floor, _new_GroupName.FLOOR, i, j))
	
	
func _init_pc() -> void:
	_add_sprite(_create_sprite(Player, _new_GroupName.PC, 0, 0))
	
	
func _init_dog() -> void:
	_add_sprite(_create_sprite(Dog, _new_GroupName.DOG, _new_GardenSize.CENTER_X, _new_GardenSize.CENTER_Y + 6))


func _add_sprite(new_sprite: Sprite) -> void:
	add_child(new_sprite)
	emit_signal("sprite_created", new_sprite)


func _create_sprite(prefab: PackedScene, group: String, x: int, y: int,
	x_offset: int = 0, y_offset: int = 0) -> Sprite:
	var new_sprite := prefab.instance() as Sprite
	new_sprite.position = _new_ConvertCoord.index_to_vector(x, y, x_offset, y_offset)
	new_sprite.add_to_group(group)
	
	return new_sprite
