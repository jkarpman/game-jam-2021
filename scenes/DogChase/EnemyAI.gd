extends Node2D


const Schedule := preload("res://scenes/DogChase/Schedule.gd")

var _ref_Schedule: Schedule

var _new_GroupName := preload("res://library/GroupName.gd").new()
var _new_ConvertCoord := preload("res://library/ConvertCoord.gd").new()
var _new_GardenSize := preload("res://library/GardenSize.gd").new()

var _pc: Sprite
var _dog: Sprite
var _down = false

func _on_Schedule_turn_started(current_sprite: Sprite, player_sprite: Sprite) -> void:
	if not current_sprite.is_in_group(_new_GroupName.DOG):
		return
		
	var target: Array
	var source: Array = _new_ConvertCoord.vector_to_array(current_sprite.position)
	target = _get_new_position(source)
	current_sprite.position = _new_ConvertCoord.index_to_vector(
				target[0], target[1])
	_ref_Schedule.end_turn()

func _get_new_position(source: Array) -> Array:
	var x: int = source[0]
	var y: int = source[1]
#
	if(x < _new_GardenSize.CENTER_X + _new_GardenSize.CENTER_X / 2):
		x += 1
	else:
		if (y == 0):
			_down = true
		if(not _down):
			y -= 1
		
	if(_down):		
		if(y != _new_GardenSize.MAX_Y):
			y += 1
		
	return [x, y]
