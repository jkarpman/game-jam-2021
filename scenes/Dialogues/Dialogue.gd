extends Control

var _new_InputName := preload("res://library/InputName.gd").new()
const dialogue_file = "res://assets/dialogue-texts/intro-1.txt"

var content = []
var dialogue_index = 0
var current_dialogue_finished = false

func _ready():
	read_dialogue_file(dialogue_file)
	load_dialogue()

func _process(delta):
	# Display end text cursor if current dialogue is finished
	$"ArrowDown".visible = current_dialogue_finished
	
	# Load next dialogue if player wishes to do so
	if Input.is_action_just_pressed(_new_InputName.UI_ACCEPT):
		load_dialogue()


func read_dialogue_file(file):
	var f = File.new()
	f.open(file, File.READ)
	var line = ""

	# Read all lines from dialogue resource file & prepare content lines for display
	while true:
		line = f.get_line()
		if not line:
			break

		var line_parts = line.split(" : ")
		content.append([line_parts[0], line_parts[1]])


func load_dialogue():
	# Load new dialogue text if some is still to be shown
	if dialogue_index < content.size():
		current_dialogue_finished = false
		# Display speaker name (centered for better display) and dialogue texte
		$NomLocuteur.bbcode_text = "[center]" + content[dialogue_index][0] + "[/center]"
		$TexteDialogue.bbcode_text = content[dialogue_index][1]

		# Display text progressively in dialogue box
		$TexteDialogue.percent_visible = 0
		$Tween.interpolate_property($TexteDialogue, "percent_visible", 0, 1, 1, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
		$Tween.start()
		
		dialogue_index += 1
	else:
		# Load dog chase scene
		get_tree().change_scene("res://scenes/DogChase/DogChase.tscn")


func _on_Tween_tween_completed(object, key):
	current_dialogue_finished = true
