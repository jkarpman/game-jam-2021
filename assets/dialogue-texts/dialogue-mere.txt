Mère de Jean : Allons Jean, je sais que Félix te manque, mais tu ne peux pas continuer à rester enfermé dans ta chambre…
Jean : Fiche-moi la paix !
Mère de Jean : Tu sais, on est toutes et tous amenés à perdre des êtres chers dans la vie… Je me rappelle à quel point j'ai souffert quand maman est morte.
Mère de Jean : Dans ces moments-là, on peut toujours compter sur nos proches. Même si Félix te manque terriblement, je suis là si tu as besoin de parler.
Jean : Arrête de faire semblant de te soucier de moi ! Tu te moques bien de ce qui m'arrive et de ce que je peux ressentir.
Mère de Jean : Non, c'est faux ! Tu es mon fils, et je t'aimerai toujours. Comment peux-tu croire ça ?
Jean : Tu n'as d'yeux que pour mon grand frère ! Lui qui a tout réussi !
Jean : C'est toujours de lui que tu parles à tes amis ! Il a fait ceci, il a fait cela… Jamais un mot pour moi !
Jean : Tu n'as jamais voulu de moi. Tu me l'as même avoué. Je suis un « accident ». Un seul enfant te suffisait.
Mère de Jean : …
Jean : Tu vois, tu ne nies même pas. Tu fais semblant de t'occuper de moi parce que tu n'assumes pas ton désintérêt ! Mais je sais ce que tu ressens véritablement.
Mère de Jean : Tu deviens ridicule ! Tu ne te souviens pas de toutes les fois où nous nous sommes promenés au parc ? Où nous sommes partis en vacances en famille ?
Mère de Jean : Tous les moments passés ensemble… Les attentions que j'ai eues pour toi… Ça ne signifie rien à tes yeux ?
Mère de Jean : Quand tu étais malade, je restais toujours à ton chevet, quitte à rater des jours de travail ! Crois-tu vraiment que j'aurais fait tout ça si je ne t'aimais pas ?
Jean : Ce n'est qu'une imposture !
Mère de Jean : Une imposture ? Quelle preuve devrais-je te donner pour que tu acceptes enfin le fait que je t'aime autant que ton frère ?
Mère de Jean : Quand donc cesseras-tu de te complaire dans ton malheur, Jean ? Il n'y a rien de bon à t'imaginer que tout le monde t'en veut.
Jean : Laisse-moi tranquille…